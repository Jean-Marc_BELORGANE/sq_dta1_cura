*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
    # Open firrfox browser #
    Open Browser    ${url}    ${browser}

Test Teardown
    Close Browser

*** Test Cases ***
JMB_CURA_Healthcare_Service
	[Setup]	Test Setup

	Given L'utilisateur est sur la pages d'accueil
	When L'utilisateur souhaite prendre un RDV
	Then La page de connexion s'affiche
	When L'utilisateur se connecte
	Then L'utilisateur est connecté sur la page de rendez-vous

	[Teardown]	Test Teardown
